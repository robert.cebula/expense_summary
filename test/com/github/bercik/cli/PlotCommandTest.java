package com.github.bercik.cli;

import com.github.bercik.plot.TimeValuePlot;
import com.github.bercik.transactions.Transactions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PlotCommandTest {
    @Mock
    private TransactionReaderRunner transactionReaderRunner;

    @Mock
    private TransactionPlotRunner transactionPlotRunner;

    @Mock
    private TimeValuePlot timeValuePlot;

    @Test
    public void doesNotThrowException_whenParsingParameters() throws Exception {
        //given
        List<String> inputText = Arrays.asList("input.xls --bank nest --format xls".split("\\s+"));
        var transactionsToReturn = new Transactions(new SimpleDateFormat(), null);
        when(transactionReaderRunner.getTransactions(any(), any())).thenReturn(transactionsToReturn);
        when(transactionPlotRunner.createTimeValuePlot(any())).thenReturn(timeValuePlot);

        PlotCommand sut = new PlotCommand(transactionReaderRunner, transactionPlotRunner);

        //when
        var result = sut.execute(inputText);

        //then
        //TODO: replace with assertDoesNotThrow​() from JUnit 5 (?)
        assertThat(result.equals(""));
    }
}