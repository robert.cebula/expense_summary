package com.github.bercik.reader;

import com.github.bercik.reader.nest.NestXlsTransactionsReader;
import com.github.bercik.transactions.Transaction;
import com.github.bercik.transactions.Transactions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class NestXlsReaderTest {

    @Test
    public void testReadTransactions_whenFileHas0Transactions() throws Exception {
        // given
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd");
        NestXlsTransactionsReader sut = new NestXlsTransactionsReader(dateFormatter);
        var inputStream = getFileInputStream("xls/nest_xls_empty.xls");

        // when
        Transactions result = sut.readTransactions(inputStream);

        // then
        Transactions expected = new Transactions(dateFormatter, null);
        assertThat(result).isEqualTo(expected);
    }

    @Test(expected = TransactionsCreationException.class)
    public void throwsTransactionsCreationException_whenColumnNameIsNotFound() throws Exception {
        // given
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd");
        NestXlsTransactionsReader sut = new NestXlsTransactionsReader(dateFormatter);
        var inputStream = getFileInputStream("xls/nest_xls_with_changed_column_name.xls");

        // when

        Transactions result = sut.readTransactions(inputStream);
    }

    @Test
    public void testReadTransactions_whenFileHas2Transactions() throws Exception {
        // given
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd");
        NestXlsTransactionsReader sut = new NestXlsTransactionsReader(dateFormatter);
        var inputStream = getFileInputStream("xls/nest_xls.xls");

        // when
        Transactions result = sut.readTransactions(inputStream);

        // then
        // 0
        Transactions expected = new Transactions(dateFormatter, null);
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.APRIL, 17))
                .transactionDate(getTime(2019, GregorianCalendar.APRIL, 13))
                .name("Transakcja bezgotówkowa \n" +
                        "Nr karty ...4077 KRAKOW JANUSZ BIESIADA BISTRO 14,90PLN")
                .valueInPennies(-1490)
                .accountBalanceInPennies(127757)
                .build());

        // 1
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.APRIL, 17))
                .transactionDate(getTime(2019, GregorianCalendar.APRIL, 13))
                .name("Transakcja bezgotówkowa \n" +
                        "Nr karty ...4077 Krakow LOOD IS GOOD 07  7,00PLN")
                .valueInPennies(-700)
                .accountBalanceInPennies(129247)
                .build());

        assertThat(result).isEqualTo(expected);
    }

    private Date getTime(@SuppressWarnings("SameParameterValue") int year, @SuppressWarnings("SameParameterValue") int month, int dayOfMonth) {
        return new GregorianCalendar(year, month, dayOfMonth).getTime();
    }

    public InputStream getFileInputStream(String name)  {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
    }

}