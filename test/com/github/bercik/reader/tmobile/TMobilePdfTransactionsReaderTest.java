package com.github.bercik.reader.tmobile;

import com.github.bercik.reader.PdfReader;
import com.github.bercik.transactions.Transaction;
import com.github.bercik.transactions.Transactions;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TMobilePdfTransactionsReaderTest {
    @Mock
    private PdfReader pdfReader;

    @Test
    public void testGetTransactionsMonth() throws Exception {
        // given
        String input = "ogółem: 2020.01.01 - 2020.01.31 1 1 z 2964 Rachunek oszczędnościowo - rozliczeniowy PLN 2964 1 134,59 124,23 0,00 8 Data wyciągu: 2020.01.31 Niespłacone";
        TMobilePdfTransactionsReader sut = new TMobilePdfTransactionsReader(null, null);

        // when
        LocalDate transactionsMonth = sut.getTransactionsMonth(input);

        // then
        LocalDate expected = LocalDate.of(2020, Month.JANUARY, 1);
        assertThat(transactionsMonth).isEqualTo(expected);
    }

    @Test
    public void testReadTransactions() throws Exception {
        // given
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd");
        String inputText = readInputText();

        InputStream stream = InputStream.nullInputStream();
        when(pdfReader.readTextFromPdf(stream)).thenReturn(inputText);

        TMobilePdfTransactionsReader sut = new TMobilePdfTransactionsReader(dateFormatter, pdfReader);

        // when
        Transactions result = sut.readTransactions(stream);

        // then
        // 0
        Transactions expected = new Transactions(dateFormatter, null);
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .transactionDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .name("PRZELEW KRAJOWY 1111 ALI BABA PIERWSZY UL.GENERAŁA WEPTYCKIEGO 16 34-453 MAKOWA ZA NETFLIXA Data transakcji: 2019-02-01")
                .valueInPennies(130)
                .accountBalanceInPennies(3470)
                .build());

        // 1
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .transactionDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .name("PRZELEW KRAJOWY 2222 ALI BABA DRUGI UL.DANKÓWKI 4 M.7 31-320 MSZYNA NETFLIX Data transakcji:")
                .valueInPennies(130)
                .accountBalanceInPennies(3770)
                .build());

        // 2
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .transactionDate(getTime(2019, GregorianCalendar.FEBRUARY, 1))
                .name("PŁACĘ Z T-MOBILE USŁUGI BANKOWE 1212 Tpay.com św. Łukasz 7 m.2, 64-814 Warszawa TMUB New Internet AB-1234-CDEF dla Miejskie Przedsi ebiorstwo Komunikacyjne S.A. w Krak owie")
                .valueInPennies(-700)
                .accountBalanceInPennies(7570)
                .build());

        // 3
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .name("PRZELEW WŁASNY 1234 Jan Kowalski Lisich Lasic 10/20A, 24-368 War szawa Na dobicie Infolinia T-Mobile Usługi Bankowe dostępna jest przez cała dobę pod numerami: 19 506 z Polski (koszt połączenia lokalnego) i telefonów komórkowych (koszt połączenia według stawek operatora), +48 12 19 506 z zagranicy (koszt połączenia według stawek operatora). T-Mobile Usługi Bankowe jest częścią Alior Bank S.A ul. Łopuszańska 38D, 02-232 Warszawa; Sad Rejonowy dla m.st. Warszawy, XIII Wydział Gospodarczy KRS: 0000305178, REGON: 141387142, NIP: 1070010731; kapitał zakładowy: 1 305 187 160 PLN (opłacony w całości).")
                .valueInPennies(100000)
                .accountBalanceInPennies(127570)
                .build());

        // 4
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.JANUARY, 31))
                .name("TRANSAKCJA KARTĄ DEBETOWĄ CARREFOUR RAKOWICKA 17 KRAKOW POL  1234 XXXX XXXX 6789 Data transakcji: 2019-01-31 Kurs wymiany:   Kwota w walucie rozliczeniowej:   Kod MCC: 5411 Kwota i waluta oryginalna transakcji: 1.95PLN")
                .valueInPennies(-5)
                .accountBalanceInPennies(12475)
                .build());

        // 5
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.JANUARY, 31))
                .name("TRANSAKCJA KARTĄ DEBETOWĄ taxify.eu Tallinn EST  1234 XXXX XXXX 6789 Data transakcji: 2019-01-31 Kurs wymiany:   Kwota w walucie rozliczeniowej: Kod MCC: 4121 Kwota i waluta oryginalna transakcji: 1.22PLN")
                .valueInPennies(-78)
                .accountBalanceInPennies(12553)
                .build());

        // 6
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.JANUARY, 31))
                .name("TRANSAKCJA KARTĄ DEBETOWĄ taxify.eu Tallinn EST  1234 XXXX XXXX 6789 Data transakcji: 2019-01-31 Kurs wymiany:   Kwota w walucie rozliczeniowej: Kod MCC: 4121 Kwota i waluta oryginalna transakcji: 1.88PLN")
                .valueInPennies(-12)
                .accountBalanceInPennies(12365)
                .build());

        // 7
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.JANUARY, 31))
                .name("TRANSAKCJA KARTĄ DEBETOWĄ ZABKA Z5414 K.1 KRAKOW POL  1234 XXXX XXXX 6789 Data transakcji: 2019-01-31 Kurs wymiany:   Kwota w walucie rozliczeniowej:   Kod MCC: 5499 Kwota i waluta oryginalna transakcji: 3.90PLN Infolinia T-Mobile Usługi Bankowe dostępna jest przez cała dobę pod numerami: 19 506 z Polski (koszt połączenia lokalnego) i telefonów komórkowych (koszt połączenia według stawek operatora), +48 12 19 506 z zagranicy (koszt połączenia według stawek operatora). T-Mobile Usługi Bankowe jest częścią Alior Bank S.A ul. Łopuszańska 38D, 02-232 Warszawa; Sad Rejonowy dla m.st. Warszawy, XIII Wydział Gospodarczy KRS: 0000305178, REGON: 141387142, NIP: 1070010731; kapitał zakładowy: 1 305 187 160 PLN (opłacony w całości).")
                .valueInPennies(-210)
                .accountBalanceInPennies(11380)
                .build());

        // 8
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 2))
                .transactionDate(getTime(2019, GregorianCalendar.JANUARY, 31))
                .name("TRANSAKCJA KARTĄ DEBETOWĄ MAMA DIDNT LIE PETER P KRAKOW POL  1234 XXXX XXXX 6789 Data transakcji: 2019-01-31 Kurs wymiany:   Kwota w walucie rozliczeniowej:   Kod MCC: 5814 Kwota i waluta oryginalna transakcji: 15.00PLN")
                .valueInPennies(-1500)
                .accountBalanceInPennies(117880)
                .build());

        // 9
        expected.addTransaction(Transaction.builder()
                .bookingDate(getTime(2019, GregorianCalendar.FEBRUARY, 5))
                .transactionDate(getTime(2019, GregorianCalendar.FEBRUARY, 5))
                .name("PRZELEW KRAJOWY 97 1030 1508 0000 0003 0024 9019 RYANAIR DAC RYANAIR DUBLIN OFFICE AIRSIDE SWORDS BUSINESS PARK, 14342 TUD2 MR JAN KOWALSKI /191328 Data transakcji: 2019-")
                .valueInPennies(107233)
                .accountBalanceInPennies(219474)
                .build());

        assertThat(result).isEqualTo(expected);
    }

    private Date getTime(@SuppressWarnings("SameParameterValue") int year, int month, int dayOfMonth) {
        return new GregorianCalendar(year, month, dayOfMonth).getTime();
    }

    public String readInputText() throws IOException {
        //noinspection ConstantConditions
        return IOUtils.toString(
                Thread.currentThread().getContextClassLoader().getResourceAsStream("pdf/tmobile_pdf_text.txt"),
                Charsets.toCharset("UTF-8")
        );
    }

}