package com.github.bercik.reader;

import com.github.bercik.reader.nest.NestXlsTransactionsReader;
import com.github.bercik.reader.tmobile.TMobilePdfTransactionsReader;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.text.SimpleDateFormat;

@Component
public class TransactionsReaderFactory {

    private final SimpleDateFormat dateFormatter;

    public TransactionsReaderFactory(SimpleDateFormat dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    public Either<String, TransactionsReader> create(@Nonnull Bank bank, @Nonnull Format format) {
        if (bank.equals(Bank.Nest) && format.equals(Format.Xls))
            return Either.right(new NestXlsTransactionsReader(dateFormatter));
        if (bank.equals(Bank.TMobile) && format.equals(Format.Pdf))
            return Either.right(new TMobilePdfTransactionsReader(dateFormatter, new PdfReader()));
        return Either.left(String.format("Unknown bank and format combination: %s - %s", bank, format));
    }
}
