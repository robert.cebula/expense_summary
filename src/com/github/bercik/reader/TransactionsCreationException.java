package com.github.bercik.reader;

public class TransactionsCreationException extends RuntimeException {
    public TransactionsCreationException(String message) {
        super(message);
    }
}
