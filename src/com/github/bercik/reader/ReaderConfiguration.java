package com.github.bercik.reader;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

@Configuration
@ComponentScan("com.github.bercik.reader")
public class ReaderConfiguration {
    @Bean
    public SimpleDateFormat dateFormatter() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy.MM.dd");
        dateFormatter.setLenient(false);
        return dateFormatter;
    }
}
