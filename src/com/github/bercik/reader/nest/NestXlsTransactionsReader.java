package com.github.bercik.reader.nest;

import com.github.bercik.reader.TransactionsCreationException;
import com.github.bercik.reader.TransactionsReader;
import com.github.bercik.transactions.Transaction;
import com.github.bercik.transactions.Transactions;
import com.google.common.collect.Streams;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class NestXlsTransactionsReader implements TransactionsReader {
    private final SimpleDateFormat dateFormatter;

    @Override
    public Transactions readTransactions(InputStream inputStream) throws IOException {
        final HSSFSheet sheet = new HSSFWorkbook(inputStream).getSheetAt(0);
        final Iterator<Row> rowIterator = sheet.iterator();

        final int summaryRows = 6;
        for(int i = 0; i < summaryRows; ++i)
        {
            rowIterator.next();
        }

        var headerRow = rowIterator.next();
        var transactionsToAdd = extractTransactions(headerRow, () -> rowIterator);

        var transactions = new Transactions(dateFormatter, null);
        for (var transaction : transactionsToAdd) {
            transactions.addTransaction(transaction);
        }
        return transactions;
    }

    public NestXlsTransactionsReader(SimpleDateFormat dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    private Iterable<Transaction> extractTransactions(Row header, Iterable<Row> transactionRows) {
        TransactionIndexes indexes = TransactionIndexes.toTransactionIndexes(header);

        List<Transaction> nonEmptyTransactions = new ArrayList<>();
        for(var row : transactionRows){
            if(isRowEmpty(row)) {
                continue;
            }

            Transaction transaction = toTransaction(indexes, row);
            nonEmptyTransactions.add(transaction);
        }
        return nonEmptyTransactions;
    }

    private Transaction toTransaction(TransactionIndexes indexes, Row row) {
        List<Cell> cells =
                Streams.stream(row.cellIterator())
                        .collect(Collectors.toList());

        Date bookingDate = cells.get(indexes.bookingDateIndex).getDateCellValue();
        Date transactionDate = cells.get(indexes.transactionDateIndex).getDateCellValue();
        String name = cells.get(indexes.nameIndex).getStringCellValue();

        final double valueInZlotys = cells
                .get(indexes.valueIndex)
                .getNumericCellValue();
        long valueInPennies = (long) (100 * valueInZlotys);
        final double accountBalanceInZlotys = cells
                .get(indexes.accountBalanceIndex)
                .getNumericCellValue();
        long accountBalanceInPennies = (long) (100 * accountBalanceInZlotys);

        return Transaction.builder()
                .bookingDate(bookingDate)
                .transactionDate(transactionDate)
                .name(name)
                .valueInPennies(valueInPennies)
                .accountBalanceInPennies(accountBalanceInPennies)
                .build();
    }

    private boolean isRowEmpty(Row row) {
        if (row == null) {
            return true;
        }
        if (row.getLastCellNum() <= 0) {
            return true;
        }
        for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
            Cell cell = row.getCell(cellNum);
            if (isCellEmpty(cell)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCellEmpty(Cell cell) {
        return cell != null && cell.getCellType() != CellType.BLANK && StringUtils.isNotBlank(cell.toString());
    }

    private static class TransactionIndexes{
        TransactionIndexes(int bookingDateIndex, int transactionDateIndex, int nameIndex, int valueIndex, int accountBalanceIndex) {
            this.bookingDateIndex = bookingDateIndex;
            this.transactionDateIndex = transactionDateIndex;
            this.nameIndex = nameIndex;
            this.valueIndex = valueIndex;
            this.accountBalanceIndex = accountBalanceIndex;
        }
        final int bookingDateIndex;
        final int transactionDateIndex;
        final int nameIndex;
        final int valueIndex;
        final int accountBalanceIndex;

        private static TransactionIndexes toTransactionIndexes(Row row) {
            Supplier<Stream<Cell>> headerCells = () -> Streams.stream(row.cellIterator());

            int bookingDateIndex = columnIndex(headerCells, "Data księgowania");
            int transactionDateIndex = columnIndex(headerCells, "Data operacji");
            int nameIndex = columnIndex(headerCells, "Tytuł operacji");
            int valueIndex = columnIndex(headerCells, "Kwota");
            int accountBalanceIndex = columnIndex(headerCells, "Saldo po operacji");

            return new TransactionIndexes(bookingDateIndex, transactionDateIndex, nameIndex, valueIndex, accountBalanceIndex);
        }

        private static int columnIndex(Supplier<Stream<Cell>> headerCells, String columnHeader) {
            return headerCells
                    .get()
                    .filter(cell -> cell.getStringCellValue().equals(columnHeader))
                    .findFirst()
                    .orElseThrow(() -> new TransactionsCreationException(String.format("Column %s not found", columnHeader)))
                    .getColumnIndex();
        }
    }
}