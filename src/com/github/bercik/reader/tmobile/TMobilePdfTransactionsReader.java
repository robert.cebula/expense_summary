package com.github.bercik.reader.tmobile;

import com.github.bercik.reader.PdfReader;
import com.github.bercik.reader.TransactionsReader;
import com.github.bercik.transactions.Transaction;
import com.github.bercik.transactions.Transactions;
import com.google.common.annotations.VisibleForTesting;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TMobilePdfTransactionsReader implements TransactionsReader {

    private static final int MAX_NAME_LENGTH = 8;
    public static final Pattern MONTHS_PATTERN = Pattern.compile("(\\d{4}\\.\\d{2}\\.\\d{2})\\s-\\s\\d{4}\\.\\d{2}\\.\\d{2}");

    private final SimpleDateFormat dateFormatter;
    private final PdfReader pdfReader;

    public TMobilePdfTransactionsReader(SimpleDateFormat dateFormatter, PdfReader pdfReader) {
        this.dateFormatter = dateFormatter;
        this.pdfReader = pdfReader;
    }

    public Transactions readTransactions(InputStream inputStream) throws IOException {
        var inputText = pdfReader.readTextFromPdf(inputStream);
        inputText = normalizeText(inputText);

        LocalDate transactionsMonth = getTransactionsMonth(inputText);
        List<Block> blocks = getBlocks(inputText);

        return processBlocks(blocks, transactionsMonth);
    }

    @VisibleForTesting
    LocalDate getTransactionsMonth(String inputText) {
        Matcher matcher = MONTHS_PATTERN.matcher(inputText);

        if (matcher.find()) {
            return LocalDate.parse(matcher.group(1), DateTimeFormatter.ofPattern("yyyy.MM.d"));
        }

        throw new RuntimeException("Cannot find transactions month");
    }

    private Transactions processBlocks(List<Block> blocks, LocalDate transactionsMonth) {
        Transactions transactions = new Transactions(dateFormatter, transactionsMonth);

        for (Block block : blocks) {
            transactions.addTransaction(processBlock(block));
        }

        return transactions;
    }

    private Transaction processBlock(Block block) {
        String name;
        int nameLengthCounter = 0;
        StringBuilder tmpName = new StringBuilder();
        int valueInPennies = 0;
        int accountBalanceInPennies = 0;

        State state = State.FIRST_PART_OF_NAME;

        for (String word : block.getWords()) {
            switch (state) {
                case FIRST_PART_OF_NAME:
                    Optional<Integer> value = getValueInPennies(word);
                    if (value.isPresent()) {
                        valueInPennies = value.get();
                        state = State.ACCOUNT_BALANCE_IN_PENNIES;
                    } else {
                        tmpName.append(word).append(" ");
                        nameLengthCounter++;
                    }
                    break;
                case ACCOUNT_BALANCE_IN_PENNIES:
                    accountBalanceInPennies = getValueInPennies(word).orElse(0);
                    state = State.DIGITS;
                    break;
                case DIGITS:
                    if (!containsOnlyDigits(word)) {
                        tmpName.append(word).append(" ");
                        state = State.SECOND_PART_OF_NAME;
                    }
                    break;
                case SECOND_PART_OF_NAME:
                    if (containsOnlyDigitsAndDash(word) || nameLengthCounter > MAX_NAME_LENGTH) {
                        state = State.OTHERS;
                        break;
                    }
                    tmpName.append(word).append(" ");
                    nameLengthCounter++;
                    break;
            }
        }

        name = tmpName.substring(0, tmpName.length() - 1);
        name = name.replaceAll("\u00A0", " ");

        return Transaction.builder()
                .bookingDate(block.getFirstDate())
                .transactionDate(block.getSecondDate())
                .name(name)
                .valueInPennies(valueInPennies)
                .accountBalanceInPennies(accountBalanceInPennies)
                .build();
    }

    private boolean containsOnlyDigits(String word) {
        return word.matches("^[0-9]+$");
    }

    private boolean containsOnlyDigitsAndDash(String word) {
        return word.matches("^[0-9-]+$");
    }

    private Optional<Integer> getValueInPennies(String word) {
        // We do this because if amount is at least 1000 it is splitted using this character (it looks like space)
        // 1 000, 2 340 etc.
        word = word.replaceAll("\u00A0", "");

        Pattern pattern = Pattern.compile("^(-?)(\\d+),(\\d{2})$");
        Matcher matcher = pattern.matcher(word);
        if (matcher.find()) {
            String minus = matcher.group(1);
            String zloty = matcher.group(2);
            String pennies = matcher.group(3);

            int iMinus = minus.equals("-") ? -1 : 1;
            int iZloty = Integer.parseInt(zloty);
            int iPennies = Integer.parseInt(pennies);

            int valueInPennies = iMinus * iZloty * 100 + iPennies;

            return Optional.of(valueInPennies);
        }

        return Optional.empty();
    }

    private List<Block> getBlocks(String text) {
        StringBuilder word = new StringBuilder();
        Date firstDate = null;
        Date secondDate = null;
        boolean inBlock = false;
        boolean shouldNextBeSecondDate = false;
        List<String> wordsInBlock = new ArrayList<>();
        List<Block> blocks = new ArrayList<>();

        for (char c : text.toCharArray()) {
            if (c == ' ') {
                Optional<Date> date = getDate(word.toString());

                if (shouldNextBeSecondDate && date.isEmpty()) {
                    firstDate = null;
                }
                shouldNextBeSecondDate = false;

                if (date.isPresent()) {
                    if (inBlock) {
                        blocks.add(new Block(firstDate, secondDate, wordsInBlock));

                        inBlock = false;
                        firstDate = null;
                        secondDate = null;
                        wordsInBlock = new ArrayList<>();
                    }

                    if (firstDate == null) {
                        firstDate = date.get();
                        shouldNextBeSecondDate = true;
                    } else {
                        secondDate = date.get();
                        inBlock = true;
                    }
                } else if (inBlock) {
                    wordsInBlock.add(word.toString());
                }

                word = new StringBuilder();
            } else {
                word.append(c);
            }
        }

        return blocks;
    }

    private Optional<Date> getDate(String word) {
        try {
            return Optional.of(dateFormatter.parse(word));
        } catch (ParseException e) {
            return Optional.empty();
        }
    }

    private String normalizeText(String text) {
        text = text.replaceAll("\n", " ");
        text = text.replaceAll("\\s+", " ");
        return text;
    }

    private enum State {
        FIRST_PART_OF_NAME,
        VALUE_IN_PENNIES,
        ACCOUNT_BALANCE_IN_PENNIES,
        DIGITS,
        SECOND_PART_OF_NAME,
        OTHERS
    }

    private class Block {
        private final Date firstDate;
        private final Date secondDate;
        private final List<String> words;

        Block(Date firstDate, Date secondDate, List<String> words) {
            this.firstDate = firstDate;
            this.secondDate = secondDate;
            this.words = words;
        }

        Date getFirstDate() {
            return firstDate;
        }

        Date getSecondDate() {
            return secondDate;
        }

        List<String> getWords() {
            return words;
        }

        @Override
        public String toString() {
            return "Block{" +
                    "firstDate=" + firstDate +
                    ", secondDate=" + secondDate +
                    ", words=" + words +
                    '}';
        }
    }

}
