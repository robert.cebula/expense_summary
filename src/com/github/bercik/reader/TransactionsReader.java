package com.github.bercik.reader;

import com.github.bercik.transactions.Transactions;

import java.io.IOException;
import java.io.InputStream;

public interface TransactionsReader {
    /**
     *
     * @param input
     * @return transactions
     * @throws TransactionsCreationException when transactions cannot be read from source
     * @throws IOException
     */
    Transactions readTransactions(InputStream input) throws IOException;
}
