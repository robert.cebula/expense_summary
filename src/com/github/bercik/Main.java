package com.github.bercik;

import com.github.bercik.cli.RootCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = null;

        try {
            logger.info("STARTING THE APPLICATION");

            context = new AnnotationConfigApplicationContext(GlobalConfiguration.class);

            RootCommand rootCommand = context.getBean(RootCommand.class);
            System.out.print(rootCommand.run(Arrays.asList(args)));
        } catch (Throwable error) {
            final String errorMessage = error.getMessage();
            logger.error(errorMessage);
            System.err.println(errorMessage);
        } finally {
            if (context != null) {
                context.close();
            }

            logger.info("APPLICATION FINISHED");
        }
    }
}