package com.github.bercik.summary;

public class IncomeTransactionSummary {
    private final String name;
    private final long valueInPennies;
    private final int numberOfTransactions;

    IncomeTransactionSummary(String name, long valueInPennies, int numberOfTransactions) {
        this.name = name;
        this.valueInPennies = valueInPennies;
        this.numberOfTransactions = numberOfTransactions;
    }

    public String name() {
        return name;
    }

    public long valueInPennies() {
        return valueInPennies;
    }

    public int numberOfTransactions() {
        return numberOfTransactions;
    }
}
