package com.github.bercik.summary;

import com.github.bercik.transactions.Transaction;
import com.github.bercik.transactions.Transactions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionsSummary {

    private final long totalSpendInPennies;
    private final long totalIncomeInPennies;
    private final List<SpendTransactionSummary> transactionsOrderedBySpend;
    private final List<IncomeTransactionSummary> transactionsOrderedByIncome;

    private TransactionsSummary(Transactions transactions) {
        long totalSpendInPennies = 0;
        long totalIncomeInPennies = 0;

        Map<String, SpendTransactionSummary> spendTransactionsByName = new HashMap<>();
        Map<String, IncomeTransactionSummary> incomeTransactionsByName = new HashMap<>();

        for (Transaction transaction : transactions) {
            long valueInPennies = transaction.getValueInPennies();
            String name = transaction.getName();
            if (valueInPennies > 0) {
                totalIncomeInPennies += valueInPennies;
                IncomeTransactionSummary its = incomeTransactionsByName.getOrDefault(name, new IncomeTransactionSummary(
                        name,
                        0L,
                        0));
                its = new IncomeTransactionSummary(
                        name,
                        its.valueInPennies() + valueInPennies,
                        its.numberOfTransactions() + 1);
                incomeTransactionsByName.put(name, its);
            } else {
                totalSpendInPennies += -valueInPennies;
                SpendTransactionSummary sts = spendTransactionsByName.getOrDefault(name, new SpendTransactionSummary(
                        name,
                        0L,
                        0));
                sts = new SpendTransactionSummary(
                        name,
                        sts.valueInPennies() + -valueInPennies,
                        sts.numberOfTransactions() + 1);
                spendTransactionsByName.put(name, sts);
            }
        }

        this.totalIncomeInPennies = totalIncomeInPennies;
        this.totalSpendInPennies = totalSpendInPennies;

        this.transactionsOrderedByIncome = new ArrayList<>(incomeTransactionsByName.values());
        transactionsOrderedByIncome.sort(Comparator.comparingLong(IncomeTransactionSummary::valueInPennies));

        this.transactionsOrderedBySpend = new ArrayList<>(spendTransactionsByName.values());
        transactionsOrderedBySpend.sort(Comparator.comparingLong(SpendTransactionSummary::valueInPennies));
    }

    public static TransactionsSummary from(Transactions transactions) {
        return new TransactionsSummary(transactions);
    }

    public long totalSpendInPennies() {
        return totalSpendInPennies;
    }

    public long totalIncomeInPennies() {
        return totalIncomeInPennies;
    }

    public List<SpendTransactionSummary> transactionsOrderedBySpend() {
        return transactionsOrderedBySpend;
    }

    public List<IncomeTransactionSummary> transactionsOrderedByIncome() {
        return transactionsOrderedByIncome;
    }
}
