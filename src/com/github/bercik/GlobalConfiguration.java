package com.github.bercik;

import com.github.bercik.cli.CliConfiguration;
import com.github.bercik.reader.ReaderConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.github.bercik")
@Import({
        ReaderConfiguration.class,
        CliConfiguration.class
})
public class GlobalConfiguration {
}
