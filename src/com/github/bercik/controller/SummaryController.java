package com.github.bercik.controller;

import com.github.bercik.cli.TransactionReaderRunner;
import com.github.bercik.summary.IncomeTransactionSummary;
import com.github.bercik.summary.SpendTransactionSummary;
import com.github.bercik.summary.TransactionsSummary;
import com.github.bercik.transactions.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SummaryController {

    @Autowired
    private TransactionReaderRunner transactionReaderRunner;

    @GetMapping("/summary")
    public String summaryIndex() {
        return "summary_index";
    }

    @PostMapping("/summary")
    public String getSummaryOfPdfFile(Model model,
                                      @RequestParam("file") MultipartFile file) throws IOException {
        Transactions transactions = transactionReaderRunner.getTransactionsForTmobile(file.getInputStream());

        TransactionsSummary transactionsSummary = TransactionsSummary.from(transactions);
        model.addAttribute("fileName", file.getOriginalFilename());
        model.addAttribute("transactionsMonth", getTransactionsMonth(transactions));
        model.addAttribute("monthlyBalance", getMonthlyBalance(transactionsSummary));
        model.addAttribute("totalIncome", toZloty(transactionsSummary.totalIncomeInPennies()));
        model.addAttribute("incomes", toIncomes(transactionsSummary.transactionsOrderedByIncome()));
        model.addAttribute("totalSpend", toZloty(transactionsSummary.totalSpendInPennies()));
        model.addAttribute("spends", toSpends(transactionsSummary.transactionsOrderedBySpend()));
        return "summary";
    }

    private String getTransactionsMonth(Transactions transactions) {
        LocalDate transactionsMonth = transactions.transactionsMonth();
        return transactionsMonth.format(DateTimeFormatter.ofPattern("MMMM yyyy"));
    }

    private String getMonthlyBalance(TransactionsSummary transactionsSummary) {
        long diff = transactionsSummary.totalIncomeInPennies() - transactionsSummary.totalSpendInPennies();
        return toZloty(diff);
    }

    private List<Spend> toSpends(List<SpendTransactionSummary> transactionsOrderedBySpend) {
        List<SpendTransactionSummary> reversed = new ArrayList<>();
        for (var transaction : transactionsOrderedBySpend) {
            reversed.add(0, transaction);
        }

        return reversed.stream()
                .map(this::toSpend)
                .collect(Collectors.toList());
    }

    private Spend toSpend(SpendTransactionSummary spendTransactionSummary) {
        return new Spend(
                spendTransactionSummary.name(),
                toZloty(spendTransactionSummary.valueInPennies()),
                Integer.toString(spendTransactionSummary.numberOfTransactions())
        );
    }

    private List<Income> toIncomes(List<IncomeTransactionSummary> transactionsOrderedByIncome) {
        List<IncomeTransactionSummary> reversed = new ArrayList<>();
        for (var transaction : transactionsOrderedByIncome) {
            reversed.add(0, transaction);
        }

        return reversed.stream()
                .map(this::toIncome)
                .collect(Collectors.toList());
    }

    private Income toIncome(IncomeTransactionSummary incomeTransactionSummary) {
        return new Income(
                incomeTransactionSummary.name(),
                toZloty(incomeTransactionSummary.valueInPennies()),
                Integer.toString(incomeTransactionSummary.numberOfTransactions())
        );
    }

    private String toZloty(long totalSpendInPennies) {
        return totalSpendInPennies / 100 + "." + Math.abs(totalSpendInPennies % 100);
    }
}
