package com.github.bercik.controller;

public class Income {
    private final String name;
    private final String value;
    private final String numberOfTransactions;

    public Income(String name, String value, String numberOfTransactions) {
        this.name = name;
        this.value = value;
        this.numberOfTransactions = numberOfTransactions;
    }

    public String name() {
        return name;
    }

    public String value() {
        return value;
    }

    public String numberOfTransactions() {
        return numberOfTransactions;
    }
}
