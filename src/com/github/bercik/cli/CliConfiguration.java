package com.github.bercik.cli;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan("com.github.bercik.cli")
public class CliConfiguration {
}
