package com.github.bercik.cli;

import com.github.bercik.transactions.Transactions;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConvertCommand extends AbstractCommand {

    private final TransactionReaderRunner transactionReaderRunner;

    public ConvertCommand(TransactionReaderRunner transactionReaderRunner) {
        super("convert");
        this.transactionReaderRunner = transactionReaderRunner;
    }

    @Override
    String execute(List<String> args) throws Exception {
        assertArgs(args);

        String inputFilepath = args.get(0);

        var parameters = Parameters
                .parse(args)
                .orElseThrow(() -> new BadCliArgumentsException("invalid parameters"));

        Transactions transactions = transactionReaderRunner.getTransactions(inputFilepath, parameters);

        return transactions.toJsonString() + "\n";
    }

    private void assertArgs(List<String> args) throws BadCliArgumentsException {
        if (args.size() <= 0) {
            String errorMessage = "You must provide input_file\n";
            errorMessage += "  Usage: " + name() + " " + help();

            throw new BadCliArgumentsException(errorMessage);
        }
    }

    @Override
    public String help() {
        return "[input_file] - converts given pdf file to json and prints to standard output";
    }
}
