package com.github.bercik.cli;

import com.github.bercik.reader.Bank;
import com.github.bercik.reader.Format;
import io.vavr.control.Either;

class TransactionReaderParametersParser {

    static Either<String, Bank> parseBank(String bankName) {
        if (bankName.equals("nest"))
            return Either.right(Bank.Nest);
        else if (bankName.equals("tmobile"))
            return Either.right(Bank.TMobile);
        return Either.left(String.format("unknown bank %s", bankName));
    }

    static Either<String, Format> parseFormat(String formatName) {
        if (formatName.equals("xls"))
            return Either.right(Format.Xls);
        else if (formatName.equals("pdf"))
            return Either.right(Format.Pdf);
        return Either.left(String.format("unknown format %s", formatName));
    }
}
