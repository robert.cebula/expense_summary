package com.github.bercik.cli;

import com.github.bercik.adapter.SpendEachDayAdapter;
import com.github.bercik.plot.TimeValuePlot;
import com.github.bercik.transactions.Transactions;
import org.springframework.stereotype.Component;

@Component
class TransactionPlotRunner {
    private static final int PLOT_WIDTH = 1200;
    private static final int PLOT_HEIGHT = 800;

    TimeValuePlot createTimeValuePlot(Transactions transactions) {
        TimeValuePlot.ChartMetadata chartMetadata = new TimeValuePlot.ChartMetadata()
                .title("each day spending")
                .timeAxisLabel("day")
                .valueAxisLabel("spending [zł]");
        TimeValuePlot.PlotMetadata plotMetadata =
                new TimeValuePlot.PlotMetadata(new TimeValuePlot.PlotDimensions(PLOT_WIDTH, PLOT_HEIGHT))
                        .applicationTitle("each day spending")
                        .chartMetadata(chartMetadata);
        TimeValuePlot.PlotShowingOptions plotShowingOptions =
                new TimeValuePlot.PlotShowingOptions().moneyShowing(TimeValuePlot.PlotShowingOptions.MoneyShowing.ZLOTYS);
        TimeValuePlot.PlotData plotData = new SpendEachDayAdapter().adapt(transactions);
        return new TimeValuePlot(plotData, plotMetadata, plotShowingOptions);
    }
}
