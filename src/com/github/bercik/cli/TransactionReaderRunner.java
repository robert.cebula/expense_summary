package com.github.bercik.cli;

import com.github.bercik.reader.Bank;
import com.github.bercik.reader.Format;
import com.github.bercik.reader.TransactionsReaderFactory;
import com.github.bercik.transactions.Transactions;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.Patterns.$None;
import static io.vavr.Patterns.$Some;

@Component
public class TransactionReaderRunner {
    private final TransactionsReaderFactory transactionsReaderFactory;

    public TransactionReaderRunner(TransactionsReaderFactory transactionsReaderFactory) {
        this.transactionsReaderFactory = transactionsReaderFactory;
    }

    public Transactions getTransactionsForTmobile(InputStream inputStream) throws IOException {
        @SuppressWarnings("Convert2MethodRef")
        var reader = transactionsReaderFactory.create(Bank.TMobile, Format.Pdf)
                .getOrElseThrow(error -> new IllegalArgumentException(error));

        return reader.readTransactions(inputStream);
    }

    public Transactions getTransactionsForTmobile(String inputFilepath) throws IOException {
        @SuppressWarnings("Convert2MethodRef")
        var inputStream = new FileInputStream(inputFilepath);

        return getTransactionsForTmobile(inputStream);
    }

    Transactions getTransactions(String inputFilepath, Parameters parameters) throws BadCliArgumentsException, IOException {
        Bank bank = getBank(parameters);
        Format format = getFormat(inputFilepath, parameters);

        @SuppressWarnings("Convert2MethodRef")
        var reader = transactionsReaderFactory.create(bank, format)
                .getOrElseThrow(error -> new IllegalArgumentException(error));

        var inputStream = new FileInputStream(inputFilepath);

        return reader.readTransactions(inputStream);
    }

    private Format getFormat(String inputFilepath, Parameters parameters) throws BadCliArgumentsException {
        var formatName = parameters.getParameterValue("format");
        return Match(formatName).of(
                Case($Some($()), formatString ->
                        TransactionReaderParametersParser.parseFormat(formatString)
                                .mapLeft(__ -> String.format("provided format %s is not supported", formatString))
                ),
                Case($None(), () ->
                        TransactionReaderParametersParser.parseFormat(FilenameUtils.getExtension(inputFilepath))
                                .mapLeft(__ -> "file extension is not supported")
                )
        ).getOrElseThrow(BadCliArgumentsException::new);
    }

    private Bank getBank(Parameters parameters) throws BadCliArgumentsException {
        var bankName = parameters.getParameterValue("bank");

        return bankName.toEither("no --bank flag provided")
                .flatMap(TransactionReaderParametersParser::parseBank)
                .getOrElseThrow(BadCliArgumentsException::new);
    }
}
