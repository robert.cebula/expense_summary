package com.github.bercik.cli;

import io.vavr.control.Option;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

// TODO: potentially to be replaced with some nice parsing library :)
class Parameters {
    private final Map<String, String> parsedParameters;

    private Parameters(Map<String, String> parsedParameters) {
        this.parsedParameters = parsedParameters;
    }

    Option<String> getParameterValue(String key) {
        return Option.ofOptional(Optional.ofNullable(parsedParameters.get(key)));
    }

    static Optional<Parameters> parse(Iterable<String> input) {
        var isParsingParameter = false;
        String key = null;
        Map<String, String> parsedParameters = new HashMap<>();
        for (var word : input) {
            if (word.startsWith("--") && !isParsingParameter) {
                isParsingParameter = true;
                key = word.substring(2);
            } else if (word.startsWith("--") && isParsingParameter) {
                return Optional.empty();
            } else if (isParsingParameter) {
                parsedParameters.put(key, word);
                isParsingParameter = false;
            } else {
                return Optional.empty();
            }
        }
        var parameters = new Parameters(parsedParameters);
        return Optional.of(parameters);
    }
}
