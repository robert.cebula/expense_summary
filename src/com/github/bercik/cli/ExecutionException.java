package com.github.bercik.cli;

@SuppressWarnings("WeakerAccess")
public class ExecutionException extends Exception {
    ExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
