package com.github.bercik.cli;

import com.github.bercik.summary.IncomeTransactionSummary;
import com.github.bercik.summary.SpendTransactionSummary;
import com.github.bercik.summary.TransactionsSummary;
import com.github.bercik.transactions.Transactions;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Component
public class SummaryCommand extends AbstractCommand {

    private final TransactionReaderRunner transactionReaderRunner;

    SummaryCommand(TransactionReaderRunner transactionReaderRunner) {
        super("summary");
        this.transactionReaderRunner = transactionReaderRunner;
    }

    @Override
    String execute(List<String> args) throws IOException {
        assertArgs(args);

        String inputFilepath = args.get(0);

        Transactions transactions = transactionReaderRunner.getTransactionsForTmobile(inputFilepath);

        TransactionsSummary transactionsSummary = TransactionsSummary.from(transactions);

        return pretty(transactionsSummary);
    }

    private String pretty(TransactionsSummary transactionsSummary) {
        StringBuilder string = new StringBuilder();

        string.append("=========================================================================================\n");
        string.append("total income: ").append(toZloty(transactionsSummary.totalIncomeInPennies())).append(" zł\n");
        string.append("-----------------------------------------------------------------------------------------\n");
        for (IncomeTransactionSummary summary1 : reverse(transactionsSummary.transactionsOrderedByIncome())) {
            string.append(pad(summary1.name(), 60)).append("| ").append(toZloty(summary1.valueInPennies())).append(" zł\n");
        }

        string.append("=========================================================================================\n");
        string.append("total spend: ").append(toZloty(transactionsSummary.totalSpendInPennies())).append(" zł\n");
        string.append("-----------------------------------------------------------------------------------------\n");
        for (SpendTransactionSummary summary1 : reverse1(transactionsSummary.transactionsOrderedBySpend())) {
            string.append(pad(summary1.name(), 60)).append("| -").append(toZloty(summary1.valueInPennies())).append(" zł\n");
        }

        return string.toString();
    }

    private String pad(String string, @SuppressWarnings("SameParameterValue") int width) {
        if (string.length() > width) {
            return string.substring(0, width);
        }
        if (string.length() == width) {
            return string;
        }

        return string + " ".repeat(width - string.length());
    }

    private Iterable<? extends IncomeTransactionSummary> reverse(List<IncomeTransactionSummary> transactionSummaries) {
        return (Iterable<IncomeTransactionSummary>) () -> new Iterator<>() {
            private final ListIterator<IncomeTransactionSummary> iterator =
                    transactionSummaries.listIterator(transactionSummaries.size());

            @Override
            public boolean hasNext() {
                return iterator.hasPrevious();
            }

            @Override
            public IncomeTransactionSummary next() {
                return iterator.previous();
            }
        };
    }

    private Iterable<? extends SpendTransactionSummary> reverse1(List<SpendTransactionSummary> transactionSummaries) {
        return (Iterable<SpendTransactionSummary>) () -> new Iterator<>() {
            private final ListIterator<SpendTransactionSummary> iterator =
                    transactionSummaries.listIterator(transactionSummaries.size());

            @Override
            public boolean hasNext() {
                return iterator.hasPrevious();
            }

            @Override
            public SpendTransactionSummary next() {
                return iterator.previous();
            }
        };
    }

    private String toZloty(long totalSpendInPennies) {
        return totalSpendInPennies / 100 + "." + totalSpendInPennies % 100;
    }

    private void assertArgs(List<String> args) {
        assert args.size() == 1;
    }

    @Override
    public String help() {
        return "[input_file] - shows summary of given file (works only for tmobile)";
    }
}
