package com.github.bercik.cli;

import org.springframework.stereotype.Component;

@Component
public class RootCommand extends AbstractCommand {

    public RootCommand(PlotCommand plotCommand, ConvertCommand convertCommand, SummaryCommand summaryCommand) {
        super("");
        addSubCommand(plotCommand);
        addSubCommand(convertCommand);
        addSubCommand(summaryCommand);
    }
}
