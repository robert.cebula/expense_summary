package com.github.bercik.cli;

@SuppressWarnings("WeakerAccess")
public class BadCliArgumentsException extends Exception {
    BadCliArgumentsException(String message) {
        super(message);
    }
}
