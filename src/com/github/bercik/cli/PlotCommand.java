package com.github.bercik.cli;

import com.github.bercik.plot.TimeValuePlot;
import com.github.bercik.transactions.Transactions;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Component
public class PlotCommand extends AbstractCommand {
    private final TransactionReaderRunner transactionReaderRunner;
    private final TransactionPlotRunner transactionPlotRunner;

    public PlotCommand(TransactionReaderRunner transactionReaderRunner, TransactionPlotRunner transactionPlotRunner) {
        super("plot");
        this.transactionReaderRunner = transactionReaderRunner;
        this.transactionPlotRunner = transactionPlotRunner;
    }

    @Override
    String execute(List<String> args) throws Exception {
        assertArgs(args);

        String inputFilepath = args.get(0);

        var outputPathIsPresent = args.size() > 1 && !args.get(1).startsWith("--");

        var parametersSublist = outputPathIsPresent ?
                args.subList(2, args.size()) :
                args.subList(1, args.size());

        var parameters = Parameters
                .parse(parametersSublist)
                .orElseThrow(() -> new BadCliArgumentsException("invalid parameters"));

        Transactions transactions = transactionReaderRunner.getTransactions(inputFilepath, parameters);

        TimeValuePlot timeValuePlot = transactionPlotRunner.createTimeValuePlot(transactions);

        Optional<String> outputFilepath = Optional.empty();
        if (outputPathIsPresent) {
            outputFilepath = Optional.of(args.get(1));
        }

        if (outputFilepath.isPresent()) {
            try {
                timeValuePlot.saveToFileAsJpeg(outputFilepath.get());
            } catch (IOException e) {
                throw new ExecutionException(e.getMessage(), e);
            }
        } else {
            timeValuePlot.showOnScreen();
        }

        return "";
    }

    private void assertArgs(List<String> args) throws BadCliArgumentsException {
        if (args.size() <= 0) {
            String errorMessage = "You must provide input_file\n";
            errorMessage += "  Usage: " + name() + " " + help();

            throw new BadCliArgumentsException(errorMessage);
        }
    }

    @Override
    public String help() {
        return "[input_file] {output_file} - plots given pdf file and shows on screen or if given saves to " +
                "output_file in jpeg format";
    }
}
