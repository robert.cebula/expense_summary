package com.github.bercik.transactions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.Nonnull;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Transactions implements Iterable<Transaction> {
    private final List<Transaction> transactions;
    private final SimpleDateFormat dateFormatter;
    private final LocalDate transactionsMonth;

    public Transactions(SimpleDateFormat dateFormatter, LocalDate transactionsMonth) {
        this.dateFormatter = dateFormatter;
        this.transactionsMonth = transactionsMonth;
        this.transactions = new ArrayList<>();
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    public String toJsonString() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(dateFormatter);

        return mapper.writeValueAsString(transactions);
    }

    @Override
    @Nonnull
    public Iterator<Transaction> iterator() {
        return transactions.iterator();
    }

    @Override
    public void forEach(Consumer<? super Transaction> action) {
        transactions.forEach(action);
    }

    @Override
    public Spliterator<Transaction> spliterator() {
        return transactions.spliterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transactions that = (Transactions) o;

        return transactions.equals(that.transactions);
    }

    @Override
    public int hashCode() {
        return transactions.hashCode();
    }

    public LocalDate transactionsMonth() {
        return transactionsMonth;
    }
}
