<html>
    <head>
        <title>
            Expense Summary
        </title>
        <link href="/css/main.css" rel="stylesheet">
    </head>
    <body>
        <h3>File name: ${fileName}</h3>
        <h3>Transactions month: ${transactionsMonth}</h3>
        <h3>Monthly balance: ${monthlyBalance} zł</h3>

        <hr/>
        <h3>Total income: ${totalIncome} zł</h3>
        <table>
        <#list incomes as income>
            <tr><td>${income.name()}</td><td>${income.value()} zł</td><td>${income.numberOfTransactions()}</td></tr>
        </#list>
        </table>

        <hr/>
        <h3>Total spend: ${totalSpend} zł</h3>
        <table>
        <#list spends as spend>
            <tr><td>${spend.name()}</td><td>${spend.value()} zł</td><td>${spend.numberOfTransactions()}</td></tr>
        </#list>
        </table>
    </body>
</html>